package com.roshless.habitrabbittracker;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.roshless.habitrabbittracker.activities.AddReoccurringHabitActivity;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddReoccurringHabitTest {

    private String stringToBetyped;

    @Rule
    public ActivityTestRule<AddReoccurringHabitActivity> activityTestRule = new ActivityTestRule<>(AddReoccurringHabitActivity.class);


    @Before
    public void initValidString() {
        // Specify a valid string.
        stringToBetyped = "testName";
    }

    @Test
    public void addHabitShortName(){
        String HabitShortName = "short name";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitShortName), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }

    @Test
    public void addHabitLongName(){
        String HabitLongName = "very very long name";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitLongName), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }
    @Test
    public void addHabitNameMoreThan20Char(){
        String HabitNameMoreThan20Char = "zzzzzaaaaaqqqqqwwwwsxcderfv";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitNameMoreThan20Char), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }
    @Test
    public void addHabitEmptyName(){
        String HabitEmptyName = "";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitEmptyName), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }
    @Test
    public void addHabitOnlySpaceName(){
        String HabitOnlySpaceName = " ";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitOnlySpaceName), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }
    @Test
    public void addHabitFirstSpaceName(){
        String HabitFirstSpaceName = " first space";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitFirstSpaceName), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }
    @Test
    public void addHabitSpecialCharName(){
        String HabitSpecialCharName = "@ %$#! *(";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitSpecialCharName), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }
    @Test
    public void addHabitLessThan3Char(){
        String HabitLessThan3Char = "az";
        onView(withId(R.id.add_reoccurring_habit_name)).perform(typeText(HabitLessThan3Char), closeSoftKeyboard());
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
    }


}
