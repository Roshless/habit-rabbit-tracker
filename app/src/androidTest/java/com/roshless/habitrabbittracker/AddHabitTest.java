package com.roshless.habitrabbittracker;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.TimePicker;

import com.roshless.habitrabbittracker.activities.AddHabitActivity;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddHabitTest {

    private String stringToBetyped;

    @Rule
    public ActivityTestRule<AddHabitActivity> activityRule
            = new ActivityTestRule<>(AddHabitActivity.class);

    @Before
    public void initValidString() {
        // Specify a valid string.
        stringToBetyped = "testName";
    }

    @Test
    public void addHabit() {
        // wpisanie nazwy habita
        onView(withId(R.id.habitNameTextView)).perform(typeText(stringToBetyped), closeSoftKeyboard());
       //zaznaczenie obecnego dnia
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.radioButtonMedium)).perform(click());
        //onView(withId(R.id.habitTimePicker)).perform();
        onView(withId(R.id.addButton)).perform(click());

    }
    @Test
    public void addHabitEasy(){
        String habitNameEasy = "easy difficulty";
        onView(withId(R.id.habitNameTextView)).perform(typeText(habitNameEasy), closeSoftKeyboard());
        onView(withId(R.id.radioButtonEasy)).perform(click());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void addHabitMedium(){
        String habitNameMedium = "medium difficulty";
        onView(withId(R.id.habitNameTextView)).perform(typeText(habitNameMedium), closeSoftKeyboard());
        onView(withId(R.id.radioButtonMedium)).perform(click());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void addHabitHard(){
        String habitNameHard = "hard difficulty";
        onView(withId(R.id.habitNameTextView)).perform(typeText(habitNameHard), closeSoftKeyboard());
        onView(withId(R.id.radioButtonHard)).perform(click());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void addHabitWithAlarm(){
        String habitNameAlarm = "Alarm enable";
        onView(withId(R.id.habitNameTextView)).perform(typeText(habitNameAlarm), closeSoftKeyboard());
        onView(withId(R.id.switch1)).perform(click());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void addSpecialCharackter(){
        String specialChar = "@%&*#abc;";
        onView(withId(R.id.habitNameTextView)).perform(typeText(specialChar), closeSoftKeyboard());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void spaceFirst(){
        String spaceFirst = " test";
        onView(withId(R.id.habitNameTextView)).perform(typeText(spaceFirst), closeSoftKeyboard());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void emptyName(){
        String empty = "";
        onView(withId(R.id.habitNameTextView)).perform(typeText(empty), closeSoftKeyboard());
        onView(withId(R.id.habitDayPicker)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }


}

