package com.roshless.habitrabbittracker;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.roshless.habitrabbittracker.activities.ReoccurringHabitsListActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class RemoveReocurringHabitTest {

    @Rule
    public ActivityTestRule<ReoccurringHabitsListActivity> activityTestRule = new ActivityTestRule<>(ReoccurringHabitsListActivity.class);

    @Test
    public void removeReoccurringHabit() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, longClick()));
        onView(withText("Remove")).perform(click());
    }
}
