package com.roshless.habitrabbittracker;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.roshless.habitrabbittracker.activities.AddGoalActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddGoalTest {
    private String stringToBetyped;

    @Rule
    public ActivityTestRule<AddGoalActivity> activityTestRule = new ActivityTestRule<>(AddGoalActivity.class);

    @Before
    public void initValidString() {
        stringToBetyped = "testName";
    }

    @Test
    public void addHabitTestName(){
        String HabitShortName = "2";
        onView(withId(R.id.add_goal_name)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText(HabitShortName), closeSoftKeyboard());
//        onView(withId(R.id.add_goal_spinner)).perform()
//        onView(withText("nice name")).perform(click());
        onView(withId(R.id.add_goal_button)).perform(click());
    }
    @Test
    public void addHabitShortName(){
        onView(withId(R.id.add_goal_name)).perform(typeText("m"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText("5"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_button)).perform(click());
    }
    @Test
    public void addHabitLongName(){
        onView(withId(R.id.add_goal_name)).perform(typeText("hsbdfkjsdvsvdsvmddscscdddcdcd"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText("77"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_button)).perform(click());
    }
    @Test
    public void addHabitBigValueAndSpace(){
        onView(withId(R.id.add_goal_name)).perform(typeText("big value"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText("770934747"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_button)).perform(click());
    }
    @Test
    public void addHabitSpecChar(){
        onView(withId(R.id.add_goal_name)).perform(typeText("$^2#!@"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText("11"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_button)).perform(click());
    }
    @Test
    public void addHabitFirstSpace(){
        onView(withId(R.id.add_goal_name)).perform(typeText(" tg54"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText("47"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_button)).perform(click());
    }
    @Test
    public void addHabitNumbersAndLetters(){
        onView(withId(R.id.add_goal_name)).perform(typeText("tfd34 fvg54"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(typeText("32"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_button)).perform(click());
    }

}
