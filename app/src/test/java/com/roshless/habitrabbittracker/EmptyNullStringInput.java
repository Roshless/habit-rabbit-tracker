package com.roshless.habitrabbittracker;

import com.roshless.habitrabbittracker.common.HabitNameLimits;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class EmptyNullStringInput {
    @Mock
    private HabitNameLimits nameLimits;

    @Before
    public void beforeEachTest(){
        nameLimits = new HabitNameLimits();
    }

    @Test
    public void isStringEmpty_checkEmptyInput(){
        String s = "";
        assertTrue(nameLimits.isStringNotEmpty(s));
    }

    @Test
    public void isStringEmpty_checkNull() {
        String s = null;
        assertTrue(nameLimits.isStringNotEmpty(s));
    }

    @Test
    public void isStringEmpyt_normalInput1(){
        String s = "Bieganie";
        assertFalse(nameLimits.isStringNotEmpty(s));
    }

    @Test
    public void isStringEmpyt_normalInput2(){
        String s = "Pompki";
        assertFalse(nameLimits.isStringNotEmpty(s));
    }

    @Test
    public void isStringEmpyt_normalInput3(){
        String s = "Gotowanie";
        assertFalse(nameLimits.isStringNotEmpty(s));
    }

}