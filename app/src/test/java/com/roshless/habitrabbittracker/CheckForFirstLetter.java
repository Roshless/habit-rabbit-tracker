package com.roshless.habitrabbittracker;

import com.roshless.habitrabbittracker.common.HabitNameLimits;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckForFirstLetter {

    @Mock
    private HabitNameLimits nameLimits;

    @Before
    public void beforeEachTest() {
        nameLimits = new HabitNameLimits();
    }

    @Test
    public void checkForFirstLetter_space() {
        String s = "    hgadshhas";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_test1() {
        String s = "----hgadshhas";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_test2() {
        String s = "____hgadshhas";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_test3() {
        String s = "!@#$%^&*()hgadshhas";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_test4() {
        String s = "4567hgadshhas";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_test5() {
        String s = "ACVhgadshhas";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_test6() {
        String s = "asdjknasd";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForFirstLetter_space_inTheMiddle() {
        String s = "asdj nasd";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }
}
