package com.roshless.habitrabbittracker.experience;

import com.roshless.habitrabbittracker.common.Experience;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExperienceTest {

    private int userID = 1;
    private int level = 1;
    private double experiencePoints = 0;

    @Mock
    private Experience experience;

    @Before
    public void beforeEachTest() {
        experience = new Experience(userID, level, experiencePoints);
    }

    @Test
    public void toString_test() {
        assertEquals("User 1 is level 1 and has 0.0 XP.", experience.toString());
    }

    @Test
    public void getUserID_test() {
        assertTrue(experience.getUserID() == 1);
    }

    @Test
    public void getLevel_test() {
        assertTrue(experience.getLevel() == 1);
    }

    @Test
    public void getExperience_test() {
        assertEquals(0.0, experience.getExperiencePoints(),0.001);
    }

    @Test
    public void setUserID_test() {
        experience.setUserID(100);
        assertEquals(100, experience.getUserID());
    }

    @Test
    public void setLevel_test() {
        experience.setLevel(50);
        assertTrue(experience.getLevel() == 50);
    }

    @Test
    public void setExperience_test() {
        experience.setExperiencePoints(10000);
        assertTrue(experience.getExperiencePoints() == 10000);
    }

    @Test
    public void emptyContructor_test() {
        Experience experience1 = new Experience();
        assertTrue(experience1.getExperiencePoints() == 0);
        assertTrue(experience1.getLevel() == 0);
        assertTrue(experience1.getUserID() == 0);
    }
}
