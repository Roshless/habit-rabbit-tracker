package com.roshless.habitrabbittracker;

import com.roshless.habitrabbittracker.common.HabitNameLimits;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SpecialCharcters {

    @Mock
    private HabitNameLimits nameLimits;

    @Before
    public void beforeEachTest(){
        nameLimits = new HabitNameLimits();
    }

    @Test
    public void space(){
        String s = "*asdasd ";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForSpecialCharachters_star(){
        String s = "*asdasd";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForSpecialCharachters_test1(){
        String s = "aass(dsds";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForSpecialCharachters_test2(){
        String s = ")dsds";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForSpecialCharachters_test3(){
        String s = "asdsad!";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForSpecialCharachters_test4(){
        String s = "@asdsad";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForSpecialCharachters_test5(){
        String s = "a#fsdfasds";
        assertTrue(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForNumbers_test1(){
        String s = "213123123";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForNumbers_test2(){
        String s = "asdasd12312";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }

    @Test
    public void checkForNumbers_test3(){
        String s = "xcv234123";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }
    @Test
    public void checkForNumbers_test4(){
        String s = "2133asda";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }
    @Test
    public void checkForNumbers_test5(){
        String s = "123123asdasd123123";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }
    @Test
    public void checkForNumbers_test6(){
        String s = "97832879312879312";
        assertFalse(nameLimits.checkForSpecialCharachters(s));
    }

}
