package com.roshless.habitrabbittracker.common;

public class Experience {
    private int userID;
    private int level;
    private double experiencePoints;
    private double nextLevelTarget;

    public Experience() {
    }

    public Experience(int userID, int level, double experiencePoints) {
        this.userID = userID;
        this.level = level;
        this.experiencePoints = experiencePoints;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(double experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public double getNextLevelTarget() {
        return nextLevelTarget;
    }

    public void addExperience(String difficulty) {
        switch (difficulty) {
            case "Easy":
                experiencePoints += 3.0 * level / 10.0;
                break;
            case "Medium":
                experiencePoints += 5.0 * level / 10.0;
                break;
            case "Hard":
                experiencePoints += 10.0 * level / 10.0;
                break;
        }
        
        nextLevelTarget = 0.04 * (Math.pow(level, 3.0)) + 0.8 * (Math.pow(level, 2.0)) + 2.0 * level;
        if (experiencePoints >= nextLevelTarget) {
            level++;
            experiencePoints -= nextLevelTarget;
        }
    }

    public void subExperience(String difficulty) {
        switch (difficulty) {
            case "Easy":
                experiencePoints -= 3.0 * level / 10.0;
                break;
            case "Medium":
                experiencePoints -= 5.0 * level / 10.0;
                break;
            case "Hard":
                experiencePoints -= 10.0 * level / 10.0;
                break;
        }

        if (experiencePoints < 0) {
            experiencePoints = 0.0;
        }
    }

    public int getProgressBar() {
        return (int) (100 * (experiencePoints / nextLevelTarget));
    }

    @Override
    public String toString() {
        return "User " + userID +
                " is level " + level +
                " and has " + experiencePoints +
                " XP.";
    }
}
