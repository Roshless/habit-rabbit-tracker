package com.roshless.habitrabbittracker.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HabitNameLimits {
    public boolean checkForSpecialCharachters(String s) {
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        boolean b = m.find();
        return b || s.charAt(0) == ' ';
    }

    public boolean isStringNotEmpty(String s) {
        return s == null || s.equals("");
    }
}
