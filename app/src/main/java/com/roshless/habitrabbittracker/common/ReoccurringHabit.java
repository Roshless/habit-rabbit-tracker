package com.roshless.habitrabbittracker.common;

import org.threeten.bp.LocalDate;

public class ReoccurringHabit {

    private int id;

    private String name;
    private int counter;
    private LocalDate day;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public ReoccurringHabit() {

    }

    public ReoccurringHabit(String name, int counter, LocalDate day) {
        this.name = name;
        this.counter = counter;
        this.day = day;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setHabitName(String name) {
        this.name = name;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }


    public int getId() {
        return id;
    }

    public String getHabitName() {
        return name;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public String toString() {
        return "\nHabit name= " + name + " " + counter;
    }
}
