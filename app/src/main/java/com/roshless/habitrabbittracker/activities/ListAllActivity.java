package com.roshless.habitrabbittracker.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.roshless.habitrabbittracker.adapters.HabitAdapter;
import com.roshless.habitrabbittracker.database.HabitDatabaseHandler;
import com.roshless.habitrabbittracker.R;

public class ListAllActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_all_habits);

        RecyclerView habitRecylerView = (RecyclerView) findViewById(R.id.HabitListView);
        HabitDatabaseHandler dbHandler = new HabitDatabaseHandler(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        habitRecylerView.setLayoutManager(mLayoutManager);

        habitRecylerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.Adapter mAdapter = new HabitAdapter(dbHandler.getHabits());
        habitRecylerView.setAdapter(mAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.fab).setVisibility(View.GONE);

        setResult(RESULT_OK, null);
    }
}
