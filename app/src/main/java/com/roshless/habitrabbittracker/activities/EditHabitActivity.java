package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dpro.widgets.WeekdaysPicker;
import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.Habit;
import com.roshless.habitrabbittracker.common.HabitNameLimits;
import com.roshless.habitrabbittracker.database.HabitDatabaseHandler;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;

public class EditHabitActivity extends AppCompatActivity {
    private TextView name;
    private TimePicker time;
    HabitDatabaseHandler dbHandler;
    private RadioButton radioButton;
    private WeekdaysPicker weekdayPicker;
    private LocalDate lastDayDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        final String habitName = intent.getStringExtra("habit");
        setContentView(R.layout.new_habit);
        name = (TextView) findViewById(R.id.habitNameTextView);
        time = (TimePicker) findViewById(R.id.habitTimePicker);
        time.setIs24HourView(true);
        weekdayPicker = (WeekdaysPicker) findViewById(R.id.habitDayPicker);

        Button btn = (Button) findViewById(R.id.addButton);
        dbHandler = new HabitDatabaseHandler(this);

        int difficulty = setHabit(dbHandler.getHabit(habitName));
        btn.setText(getString(R.string.edit_habit_apply_changes));
        radioButton = (RadioButton) findViewById(difficulty);
        addListenerOnButtonRadio();

        final HabitNameLimits limits = new HabitNameLimits();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limits.isStringNotEmpty(name.getText().toString())) {
                    Toast.makeText(EditHabitActivity.this,
                            getString(R.string.no_habit_name_toast_message),
                            Toast.LENGTH_LONG).show();

                } else if (limits.checkForSpecialCharachters(name.getText().toString())) {
                    Toast.makeText(EditHabitActivity.this,
                            getString(R.string.wrong_characters_in_string),
                            Toast.LENGTH_LONG).show();
                } else if (weekdayPicker.getSelectedDays().isEmpty()) {
                    Toast.makeText(EditHabitActivity.this, R.string.toast_select_one_day, Toast.LENGTH_SHORT).show();
                } else {
                    dbHandler.updateHabit(habitName, name.getText().toString(),
                            radioButton.getText().toString(),
                            LocalTime.of(time.getHour(), time.getMinute()), weekdayPicker.getSelectedDays(), lastDayDone);

                    String message = getString(R.string.toast_msg_habit) + " "
                            + habitName + " "
                            + getString(R.string.edit_toast_msg_after);
                    Toast.makeText(EditHabitActivity.this, message, Toast.LENGTH_LONG).show();
                    //setResult(RESULT_OK, null);
                    finish();
                    startActivity(new Intent(getApplicationContext(), com.roshless.habitrabbittracker.activities.ListActivity.class));
                }
            }
        });
    }

    private int setHabit(Habit habit) {
        name.setText(habit.getHabitName());
        time.setHour(habit.getHabitTime().getHour());
        time.setMinute(habit.getHabitTime().getMinute());
        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
        weekdayPicker.setSelectedDays(habit.getHabitDays());
        //weekdayPicker.selectDay(habit.getHabitDays());
        String difficulty = habit.getDifficulty();
        if (difficulty.equals("Easy")) {
            rg.check(R.id.radioButtonEasy);
            return R.id.radioButtonEasy;
        } else if (difficulty.equals("Medium")) {
            rg.check(R.id.radioButtonMedium);
            return R.id.radioButtonMedium;
        } else if (difficulty.equals("Hard")) {
            rg.check(R.id.radioButtonHard);
            return R.id.radioButtonHard;
        }
        lastDayDone = habit.getLastDayDone();
        return 0;
    }

    public void addListenerOnButtonRadio() {
        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);
            }
        });
    }
}
