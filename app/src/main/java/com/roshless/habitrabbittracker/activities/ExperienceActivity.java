package com.roshless.habitrabbittracker.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.Experience;
import com.roshless.habitrabbittracker.database.ExperienceDatabaseHandler;

import java.text.DecimalFormat;

public class ExperienceActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xp_menu);

        ExperienceDatabaseHandler dbHandler = new ExperienceDatabaseHandler(this);
        TextView experience_level = (TextView) findViewById(R.id.experience_level);
        TextView experience_xp = (TextView) findViewById(R.id.experience_xp);
        Experience userExperience = dbHandler.getUserExperience(1);
        DecimalFormat df = new DecimalFormat("####0.00");

        if (!(userExperience == null)) {
            experience_level.setText(String.valueOf(userExperience.getLevel()));
            experience_xp.setText(df.format(userExperience.getExperiencePoints()));
        } else {
            experience_level.setText("0");
            experience_xp.setText("0");
        }
    }
}
