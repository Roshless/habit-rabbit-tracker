package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.HabitNameLimits;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;

import org.threeten.bp.LocalDate;

public class EditReoccurringHabitActivity extends AppCompatActivity {
    private TextView name;
    ReoccurringHabitDatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        final String habitName = intent.getStringExtra("habit");
        setContentView(R.layout.new_reoccurring_habit);

        name = (TextView) findViewById(R.id.add_reoccurring_habit_name);
        Button button = (Button) findViewById(R.id.add_reoccurring_habit_button);
        dbHandler = new ReoccurringHabitDatabaseHandler(this);
        name.setText(habitName);
        button.setText(getString(R.string.edit_habit_apply_changes));

        final HabitNameLimits limits = new HabitNameLimits();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limits.isStringNotEmpty(name.getText().toString())) {
                    Toast.makeText(EditReoccurringHabitActivity.this,
                            getString(R.string.no_habit_name_toast_message),
                            Toast.LENGTH_LONG).show();

                } else if (limits.checkForSpecialCharachters(name.getText().toString())) {
                    Toast.makeText(EditReoccurringHabitActivity.this,
                            getString(R.string.wrong_characters_in_string),
                            Toast.LENGTH_LONG).show();
                } else {
                    dbHandler.updateHabit(habitName, name.getText().toString(),
                            0, LocalDate.now());

                    String message = getString(R.string.toast_msg_habit) + " "
                            + habitName + " "
                            + getString(R.string.edit_toast_msg_after);
                    Toast.makeText(EditReoccurringHabitActivity.this, message, Toast.LENGTH_LONG).show();
                    dbHandler.close();
                    //setResult(RESULT_OK, null);
                    finish();
                    startActivity(new Intent(getApplicationContext(), ReoccurringHabitsListActivity.class));
                }
            }
        });
    }
}
