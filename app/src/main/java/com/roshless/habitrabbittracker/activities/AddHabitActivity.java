package com.roshless.habitrabbittracker.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dpro.widgets.WeekdaysPicker;
import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.Habit;
import com.roshless.habitrabbittracker.common.HabitNameLimits;
import com.roshless.habitrabbittracker.database.HabitDatabaseHandler;

import org.threeten.bp.LocalTime;

import java.util.Calendar;

public class AddHabitActivity extends AppCompatActivity {
    private TextView name;
    private TimePicker time;
    HabitDatabaseHandler dbHandler;
    private RadioButton radioButton;
    private WeekdaysPicker weekdayPicker;
    private Switch alarmSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        setContentView(R.layout.new_habit);
        name = (TextView) findViewById(R.id.habitNameTextView);
        time = (TimePicker) findViewById(R.id.habitTimePicker);

        // Set to 24h at all times.
        time.setIs24HourView(true);
        weekdayPicker = (WeekdaysPicker) findViewById(R.id.habitDayPicker);
        setWeekDayPickerOptions();

        Button btn = (Button) findViewById(R.id.addButton);
        dbHandler = new HabitDatabaseHandler(this);
        radioButton = (RadioButton) findViewById(R.id.radioButtonEasy);
        alarmSwitch = (Switch) findViewById(R.id.switch1);
        addListenerOnButtonRadio();

        final HabitNameLimits limits = new HabitNameLimits();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limits.isStringNotEmpty(name.getText().toString())) {
                    Toast.makeText(AddHabitActivity.this,
                            getString(R.string.no_habit_name_toast_message),
                            Toast.LENGTH_LONG).show();

                } else if (limits.checkForSpecialCharachters(name.getText().toString())) {
                    Toast.makeText(AddHabitActivity.this,
                            getString(R.string.wrong_characters_in_string),
                            Toast.LENGTH_LONG).show();
                } else if (weekdayPicker.getSelectedDays().isEmpty()) {
                    Toast.makeText(AddHabitActivity.this, R.string.toast_select_one_day, Toast.LENGTH_SHORT).show();
                } else if (dbHandler.habitExists(new Habit(name.getText().toString(),
                        radioButton.getText().toString(),
                        LocalTime.of(time.getHour(), time.getMinute()), weekdayPicker.getSelectedDays(), null))) {
                    Toast.makeText(AddHabitActivity.this,
                            getString(R.string.already_exists_toast_message), Toast.LENGTH_LONG).show();
                } else {
                    dbHandler.addHabit(new Habit(name.getText().toString(),
                            radioButton.getText().toString(),
                            LocalTime.of(time.getHour(), time.getMinute()), weekdayPicker.getSelectedDays(), null));

                    String message = getString(R.string.toast_msg_habit) + " "
                            + name.getText().toString() + " "
                            + getString(R.string.toast_msg_added);
                    Toast.makeText(AddHabitActivity.this, message, Toast.LENGTH_LONG).show();
                    if (alarmSwitch.isChecked()) {
                        setAlarm(time.getHour(), time.getMinute(), weekdayPicker.getSelectedDays().get(0));
                    }
                    setResult(RESULT_OK, null);
                    finish();
                }
            }
        });
    }

    private void setAlarm(int hour, int minute, int day) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.DAY_OF_WEEK, day);

        calendar.add(Calendar.MINUTE, -5);
        //calendar.add(Calendar.SECOND, 5);

        Intent intent = new Intent(this, AlarmReceiverActivity.class);
        intent.putExtra("habit_name", name.getText().toString());
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                12345, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am =
                (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                pendingIntent);
    }

    private void setWeekDayPickerOptions() {
        //weekdayPicker.setSelectOnlyOne(true);
        weekdayPicker.selectDay(0);
    }

    public void addListenerOnButtonRadio() {
        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);
            }
        });
    }
}
