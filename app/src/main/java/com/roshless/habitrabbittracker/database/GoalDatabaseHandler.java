package com.roshless.habitrabbittracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.roshless.habitrabbittracker.common.Goal;

import java.util.ArrayList;
import java.util.List;

public class GoalDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "goal_habits.db";

    private static final String TABLE_NAME = "habits_table";

    private static final String COLUMN_ID = "habit_ID";
    private static final String COLUMN_NAME = "habit_name";
    private static final String COLUMN_REOCCURRING = "habit_reoccurring_name";
    private static final String COLUMN_GOAL = "habit_goal";


    public GoalDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + COLUMN_NAME + " TEXT, " + COLUMN_REOCCURRING + " INTEGER, "
                + COLUMN_GOAL + " INTEGER )";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public List<Goal> getHabits() {
        List<Goal> returnList = new ArrayList<>();

        String query = "Select * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String habitName = cursor.getString(1);
            String habitReoccurringName = cursor.getString(2);
            String habitGoal = cursor.getString(3);

            returnList.add(new Goal(habitName, habitReoccurringName, Integer.parseInt(habitGoal)));
        }
        cursor.close();
        db.close();

        return returnList;
    }

    public Goal getHabit(String habit) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + habit + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        String habitName;
        String habitReoccurringName;
        String habitGoal;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            habitName = cursor.getString(1);
            habitReoccurringName = cursor.getString(2);
            habitGoal = cursor.getString(3);
            cursor.close();
        } else {
            return null;
        }
        db.close();

        return new Goal(habitName, habitReoccurringName, Integer.parseInt(habitGoal));
    }

    public void addHabit(Goal habit) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, (String) null);
        values.put(COLUMN_NAME, habit.getName());
        values.put(COLUMN_REOCCURRING, habit.getReoccurringName());
        values.put(COLUMN_GOAL, habit.getGoal());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public boolean habitExists(Goal habit) {
        List<Goal> habitList = getHabits();

        for (Goal lookedAt : habitList) {
            if (habit.getName().equalsIgnoreCase(lookedAt.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean habitExists(String habitName) {
        List<Goal> habitList = getHabits();

        for (Goal lookedAt : habitList) {
            if (habitName.equalsIgnoreCase(lookedAt.getName())) {
                return true;
            }
        }
        return false;
    }

    public int findHabit(String habit) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + habit + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int id = -1;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            id = Integer.parseInt(cursor.getString(0));
            cursor.close();
        }
        db.close();
        return id;
    }

    public boolean deleteHabit(int id) {
        boolean result = false;
        if (id == -1)
            return result;

        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " = '" + String.valueOf(id) + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Goal habit = new Goal();

        if (cursor.moveToFirst()) {
            habit.setId(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_NAME, COLUMN_ID + "=?",
                    new String[]{
                            String.valueOf(habit.getId())
                    });
            cursor.close();
            result = true;
        }

        db.close();
        return result;
    }

    public boolean updateHabit(String oldName, String name, String reoccurringName, int goal) {
        int id = findHabit(oldName);

        ContentValues newHabit = new ContentValues();
        newHabit.put(COLUMN_ID, id);
        newHabit.put(COLUMN_NAME, name);
        newHabit.put(COLUMN_REOCCURRING, reoccurringName);
        newHabit.put(COLUMN_GOAL, goal);

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(TABLE_NAME, newHabit, COLUMN_ID + " = " + id, null) > 0;
    }
}
