package com.roshless.habitrabbittracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.roshless.habitrabbittracker.common.Experience;

public class ExperienceDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "experience.db";

    private static final String TABLE_NAME = "experience_table";

    private static final String COLUMN_ID = "experience_ID";
    private static final String COLUMN_LEVEL = "experience_level";
    private static final String COLUMN_XP = "experience_xp";


    public ExperienceDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + COLUMN_LEVEL + " INTEGER, " + COLUMN_XP + " REAL )";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public Experience getUserExperience(int id) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " = '" + id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int userID;
        int level;
        double experience;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            userID = cursor.getInt(0);
            level = cursor.getInt(1);
            experience = cursor.getDouble(2);
            cursor.close();
        } else {
            return null;
        }
        db.close();

        return new Experience(userID, level, experience);
    }

    public void addUserExperience(Experience experience) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, (String) null);
        values.put(COLUMN_LEVEL, experience.getLevel());
        values.put(COLUMN_XP, experience.getExperiencePoints());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public boolean updateUserExperience(int id, int level, double xp) {
        ContentValues newUserExperience = new ContentValues();
        newUserExperience.put(COLUMN_ID, id);
        newUserExperience.put(COLUMN_LEVEL, level);
        newUserExperience.put(COLUMN_XP, xp);

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(TABLE_NAME, newUserExperience, COLUMN_ID + " = " + id, null) > 0;
    }
}
