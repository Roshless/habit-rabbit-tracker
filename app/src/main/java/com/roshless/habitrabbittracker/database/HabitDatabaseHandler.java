package com.roshless.habitrabbittracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.roshless.habitrabbittracker.common.Habit;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;

import java.util.ArrayList;
import java.util.List;

public class HabitDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "habits.db";

    private static final String TABLE_NAME = "habits_table";

    private static final String COLUMN_ID = "habit_ID";
    private static final String COLUMN_NAME = "habit_name";
    private static final String COLUMN_DIFFICULTY = "habit_difficulty";
    private static final String COLUMN_TIME = "habit_time";
    private static final String COLUMN_DAYS = "habit_day";
    private static final String COLUMN_LASTDAY = "habit_lastdone";


    public HabitDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + COLUMN_NAME + " TEXT, " + COLUMN_DIFFICULTY + " TEXT, "
                + COLUMN_TIME + " TEXT, " + COLUMN_DAYS + " TEXT, " + COLUMN_LASTDAY + " TEXT )";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public List<Habit> getHabits() {
        List<Habit> returnList = new ArrayList<>();

        String query = "Select * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            boolean lastDayExists;
            int id = cursor.getInt(0);
            String habitName = cursor.getString(1);
            String habitDifficulty = cursor.getString(2);
            String habitTime = cursor.getString(3);
            String habitDay = cursor.getString(4);
            String habitLastDoneString = cursor.getString(5);

            String[] days_string = habitDay.split(",");

            List<Integer> days = new ArrayList<Integer>();
            for (String element : days_string) {
                days.add(Integer.parseInt(element));
            }
            if (!(habitLastDoneString == null)) {
                LocalDate habitLastDone = LocalDate.parse(habitLastDoneString);
                returnList.add(new Habit(habitName, habitDifficulty, LocalTime.parse(habitTime), days, habitLastDone));
            } else {
                returnList.add(new Habit(habitName, habitDifficulty, LocalTime.parse(habitTime), days, null));
            }
        }
        cursor.close();
        db.close();

        return returnList;
    }

    public List<Habit> getHabitsOnDay(int day) {
        List<Habit> habits = getHabits();
        List<Habit> returnList = new ArrayList<>();
        for (Habit habit : habits) {
            if (habit.getHabitDays().contains(day)) {
                if (habit.getLastDayDone() == null) {
                    returnList.add(habit);
                } else if (habit.getLastDayDone().getMonth() != LocalDate.now().getMonth() ||
                        habit.getLastDayDone().getDayOfMonth() != LocalDate.now().getDayOfMonth()) {
                    returnList.add(habit);
                }
            }
        }
        return returnList;
    }

    public boolean isHabitToday(String habitName) {
        LocalDate today = LocalDate.now();
        Habit habit = getHabit(habitName);

        return habit.getLastDayDone().getMonth().equals(today.getMonth()) && (habit.getLastDayDone().getDayOfMonth() == today.getDayOfMonth());
    }


    public Habit getHabit(String habit) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + habit + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        List<Integer> days;
        String habitName;
        String habitDifficulty;
        String habitTime;
        String habitDay;
        String habitLastDoneString;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            habitName = cursor.getString(1);
            habitDifficulty = cursor.getString(2);
            habitTime = cursor.getString(3);
            habitDay = cursor.getString(4);
            habitLastDoneString = cursor.getString(5);

            String[] days_string = habitDay.split(",");

            days = new ArrayList<Integer>();
            for (String element : days_string) {
                days.add(Integer.parseInt(element));
            }
            cursor.close();
        } else {
            return null;
        }
        db.close();

        if (habitLastDoneString != null) {
            LocalDate habitLastDone = LocalDate.parse(habitLastDoneString);
            return new Habit(habitName, habitDifficulty, LocalTime.parse(habitTime), days, habitLastDone);
        } else {
            return new Habit(habitName, habitDifficulty, LocalTime.parse(habitTime), days, null);
        }
    }

    public void addHabit(Habit habit) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, (String) null);
        values.put(COLUMN_NAME, habit.getHabitName());
        values.put(COLUMN_DIFFICULTY, habit.getDifficulty());
        values.put(COLUMN_TIME, habit.getHabitTime().toString());
        values.put(COLUMN_DAYS, listToString(habit.getHabitDays()));
        values.put(COLUMN_LASTDAY, (String) null);

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public boolean habitExists(Habit habit) {
        List<Habit> habitList = getHabits();

        for (Habit lookedAt : habitList) {
            if (habit.getHabitName().equalsIgnoreCase(lookedAt.getHabitName())) {
                return true;
            }
        }
        return false;
    }

    public boolean habitExists(String habitName) {
        List<Habit> habitList = getHabits();

        for (Habit lookedAt : habitList) {
            if (habitName.equalsIgnoreCase(lookedAt.getHabitName())) {
                return true;
            }
        }
        return false;
    }

    public int findHabit(String habit) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + habit + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int id = -1;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            id = Integer.parseInt(cursor.getString(0));
            cursor.close();
        }
        db.close();
        return id;
    }

    public boolean deleteHabit(int id) {
        boolean result = false;
        if (id == -1)
            return result;

        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " = '" + String.valueOf(id) + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Habit habit = new Habit();

        if (cursor.moveToFirst()) {
            habit.setId(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_NAME, COLUMN_ID + "=?",
                    new String[]{
                            String.valueOf(habit.getId())
                    });
            cursor.close();
            result = true;
        }

        db.close();
        return result;
    }

    public boolean updateHabit(String oldName, String name, String difficulty, LocalTime time, List<Integer> days, LocalDate date) {
        int id = findHabit(oldName);

        ContentValues newHabit = new ContentValues();
        newHabit.put(COLUMN_ID, id);
        newHabit.put(COLUMN_NAME, name);
        newHabit.put(COLUMN_DIFFICULTY, difficulty);
        newHabit.put(COLUMN_TIME, time.toString());
        newHabit.put(COLUMN_DAYS, listToString(days));
        newHabit.put(COLUMN_LASTDAY, date == null ? null : date.toString());

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(TABLE_NAME, newHabit, COLUMN_ID + " = " + id, null) > 0;
    }

    private String listToString(List<?> list) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            result.append(list.get(i)).append(",");
        }
        return result.substring(0, result.length() - 1);
    }

}
