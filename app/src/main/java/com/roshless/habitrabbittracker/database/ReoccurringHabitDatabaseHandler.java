package com.roshless.habitrabbittracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.roshless.habitrabbittracker.common.ReoccurringHabit;

import org.threeten.bp.LocalDate;

import java.util.ArrayList;
import java.util.List;

public class ReoccurringHabitDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "reoccurring_habits.db";

    private static final String TABLE_NAME = "reoccurring_habits_table";

    private static final String COLUMN_ID = "habit_ID";
    private static final String COLUMN_NAME = "habit_name";
    private static final String COLUMN_COUNTER = "habit_counter";
    private static final String COLUMN_DAY = "habit_day";


    public ReoccurringHabitDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + COLUMN_NAME + " TEXT, " + COLUMN_COUNTER +
                " INTEGER, " + COLUMN_DAY + " TEXT )";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public List<ReoccurringHabit> getHabits() {
        LocalDate today = LocalDate.now();
        List<ReoccurringHabit> returnList = new ArrayList<>();

        String query = "Select * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            boolean lastDayExists;
            int id = cursor.getInt(0);
            String habitName = cursor.getString(1);
            String habitCounter = cursor.getString(2);
            String habitDay = cursor.getString(3);

            if (LocalDate.parse(habitDay).equals(today)) {
                returnList.add(new ReoccurringHabit(habitName, Integer.valueOf(habitCounter), LocalDate.parse(habitDay)));
            } else {
                returnList.add(new ReoccurringHabit(habitName, 0, LocalDate.now()));
                resetCounter(habitName);
            }
        }
        cursor.close();
        db.close();

        return returnList;
    }

    public List<String> getHabitsString() {
        List<String> toReturn = new ArrayList<>();
        List<ReoccurringHabit> habits = getHabits();

        for (ReoccurringHabit habit : habits) {
            toReturn.add(habit.getName());
        }
        return toReturn;
    }

    public ReoccurringHabit getHabit(String habit) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + habit + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        String habitName;
        String habitCounter;
        String habitDay;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            habitName = cursor.getString(1);
            habitCounter = cursor.getString(2);
            habitDay = cursor.getString(3);
            cursor.close();
        } else {
            return null;
        }
        db.close();

        return new ReoccurringHabit(habitName, Integer.valueOf(habitCounter), LocalDate.parse(habitDay));
    }

    public void addHabit(ReoccurringHabit habit) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, (String) null);
        values.put(COLUMN_NAME, habit.getHabitName());
        values.put(COLUMN_COUNTER, habit.getCounter());
        values.put(COLUMN_DAY, habit.getDay().toString());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public boolean habitExists(ReoccurringHabit habit) {
        List<ReoccurringHabit> habitList = getHabits();

        for (ReoccurringHabit lookedAt : habitList) {
            if (habit.getHabitName().equalsIgnoreCase(lookedAt.getHabitName())) {
                return true;
            }
        }
        return false;
    }

    public boolean habitExists(String habitName) {
        List<ReoccurringHabit> habitList = getHabits();

        for (ReoccurringHabit lookedAt : habitList) {
            if (habitName.equalsIgnoreCase(lookedAt.getHabitName())) {
                return true;
            }
        }
        return false;
    }

    public int findHabit(String habit) {
        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + habit + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int id = -1;

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            id = Integer.parseInt(cursor.getString(0));
            cursor.close();
        }
        db.close();
        return id;
    }

    public boolean deleteHabit(int id) {
        boolean result = false;
        if (id == -1)
            return result;

        String query = "Select * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " = '" + String.valueOf(id) + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ReoccurringHabit habit = new ReoccurringHabit();

        if (cursor.moveToFirst()) {
            habit.setId(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_NAME, COLUMN_ID + "=?",
                    new String[]{
                            String.valueOf(habit.getId())
                    });
            cursor.close();
            result = true;
        }

        db.close();
        return result;
    }

    public boolean updateHabit(String oldName, String name, int counter, LocalDate day) {
        int id = findHabit(oldName);

        ContentValues newHabit = new ContentValues();
        newHabit.put(COLUMN_ID, id);
        newHabit.put(COLUMN_NAME, name);
        newHabit.put(COLUMN_COUNTER, counter);
        newHabit.put(COLUMN_DAY, day.toString());

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(TABLE_NAME, newHabit, COLUMN_ID + " = " + id, null) > 0;
    }

    public void increaseCounter(String habitName) {
        ReoccurringHabit habit = getHabit(habitName);
        updateHabit(habit.getHabitName(), habit.getHabitName(), habit.getCounter() + 1, habit.getDay());
    }

    public void decreaseCounter(String habitName) {
        ReoccurringHabit habit = getHabit(habitName);
        updateHabit(habit.getHabitName(), habit.getHabitName(), habit.getCounter() - 1, habit.getDay());
    }

    private void resetCounter(String habitName) {
        ReoccurringHabit habit = getHabit(habitName);
        updateHabit(habit.getHabitName(), habit.getHabitName(), 0, LocalDate.now());
    }

}
