package com.roshless.habitrabbittracker.adapters;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.Goal;
import com.roshless.habitrabbittracker.common.ReoccurringHabit;
import com.roshless.habitrabbittracker.database.GoalDatabaseHandler;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;
import com.roshless.habitrabbittracker.dialogs.ListGoalActivityDialog;

import java.util.List;

public class GoalAdapter extends RecyclerView.Adapter<GoalAdapter.GoalHabitViewHolder> {
    private List<Goal> habitList;

    public GoalAdapter(List<Goal> habitList) {
        this.habitList = habitList;
    }

    @Override
    public GoalAdapter.GoalHabitViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        View habitView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.element_on_goal_habit_list, parent, false);
        return new GoalHabitViewHolder(habitView);
    }

    @Override
    public void onBindViewHolder(@NonNull GoalHabitViewHolder habitViewHolder, int position) {
        ReoccurringHabitDatabaseHandler handler = new ReoccurringHabitDatabaseHandler(habitViewHolder.itemView.getContext());
        ReoccurringHabit habit = handler.getHabit(habitList.get(position).getReoccurringName());
        handler.close();

        habitViewHolder.habitName.setText(habitList.get(position).getName());
        habitViewHolder.habitReoccurring.setText(habit.getName());

        int goal = habitList.get(position).getGoal();
        habitViewHolder.habitProgressString.setText(habitViewHolder.itemView.getContext().
                getResources().getString(R.string.goal_progress,
                habit.getCounter(),
                goal));

        habitViewHolder.habitProgressBar.setProgress((int) (100 * ((float) habit.getCounter() / (float) goal)));
    }

    @Override
    public int getItemCount() {
        return habitList.size();
    }

    public class GoalHabitViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView habitName;
        public TextView habitReoccurring;
        public TextView habitProgressString;
        public ProgressBar habitProgressBar;

        public GoalHabitViewHolder(final View view) {
            super(view);
            habitName = (TextView) view.findViewById(R.id.goal_name);
            habitReoccurring = (TextView) view.findViewById(R.id.text_habit_to_goal);
            habitProgressString = (TextView) view.findViewById(R.id.text_goal_progress);
            habitProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar_goal);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {
                    ListGoalActivityDialog dialog = new ListGoalActivityDialog(view.getContext(),
                            getAdapterPosition(), habitName.getText().toString());
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            // Worst idea I had in a long time but at least it works. This is third time
                            // I copy paste it in this project. Send help.
                            GoalDatabaseHandler dbHandler = new GoalDatabaseHandler(view.getContext());
                            habitList = dbHandler.getHabits();
                            notifyDataSetChanged();
                        }
                    });
                    return true;
                }
            });

        }
    }
}
