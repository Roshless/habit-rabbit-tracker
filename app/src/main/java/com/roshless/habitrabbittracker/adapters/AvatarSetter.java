package com.roshless.habitrabbittracker.adapters;

import android.widget.ImageView;

import com.roshless.habitrabbittracker.R;

public class AvatarSetter {
    private int level;
    private ImageView avatar;

    public AvatarSetter(int level, ImageView avatar) {
        this.level = level;
        this.avatar = avatar;
    }

    public void setImage() {
        switch (level) {
            case 1:
                avatar.setImageResource(R.drawable.icon_lvl_1);
                break;
            case 2:
                avatar.setImageResource(R.drawable.icon_lvl_2);
                break;
            case 3:
                avatar.setImageResource(R.drawable.icon_lvl_3);
                break;
            case 4:
                avatar.setImageResource(R.drawable.icon_lvl_4);
                break;
            case 5:
                avatar.setImageResource(R.drawable.icon_lvl_5);
                break;
            case 6:
                avatar.setImageResource(R.drawable.icon_lvl_6);
                break;
            case 7:
                avatar.setImageResource(R.drawable.icon_lvl_7);
                break;
            case 8:
                avatar.setImageResource(R.drawable.icon_lvl_8);
                break;
            case 9:
                avatar.setImageResource(R.drawable.icon_lvl_9);
                break;
            case 10:
                avatar.setImageResource(R.drawable.icon_lvl_10);
                break;
            default:
                avatar.setImageResource(R.drawable.icon_lvl_10);
                break;
        }
        //setImageBitmap
        //setImageDrawable
        //setImageResource
    }
}
